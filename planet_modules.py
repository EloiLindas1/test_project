import numpy as np


def get_planet_sun_distance(planet_coord):
    return np.sqrt(np.sum(np.square(planet_coord)))


def get_planet_sun_direction(planet_coord):
    planet_sun_distance = get_planet_sun_distance(planet_coord)
    return -planet_coord / planet_sun_distance


def get_acceleration_t(planet_coord, sun_mass):
    planet_sun_distance = get_planet_sun_distance(planet_coord)
    planet_sun_direction = get_planet_sun_direction(planet_coord)
    return (sun_mass / planet_sun_distance ** 2) * planet_sun_direction


def velocity_verlet(planet_coord, planet_vel, sun_mass):
    planet_sun_distance = get_planet_sun_distance(planet_coord)
    planet_sun_direction = get_planet_sun_direction(planet_coord)
    acceleration_t = get_acceleration_t(planet_coord, sun_mass)

    # update with acceleration at t
    planet_coord += planet_vel + 0.5 * acceleration_t

    planet_sun_distance = get_planet_sun_distance(planet_coord)
    planet_sun_direction = get_planet_sun_direction(planet_coord)
    acceleration_tp1 = get_acceleration_t(planet_coord, sun_mass)

    # update with acceleration at t and t+1
    planet_vel += 0.5 * (acceleration_t + acceleration_tp1)
    return planet_coord, planet_vel


def animate(i, line1, planet_coord, planet_vel, sun_mass):

    planet_coord, planet_vel = velocity_verlet(planet_coord, planet_vel, sun_mass)

    line1.set_data(planet_coord[0], planet_coord[1])  # update the data
    return (line1,)


def animate_multiple(i, lines, planet_coord, planet_vel, sun_mass):
    planet_coord, planet_vel = velocity_verlet(planet_coord, planet_vel, sun_mass)
    for i, line in enumerate(lines):
        line.set_data(planet_coord[i, 0], planet_coord[i, 1])  # update the data


def random_color():
    return np.random.rand(3)
