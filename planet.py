import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

from planet_modules import velocity_verlet, animate, animate_multiple, random_color


# setup the figure
fig, ax = plt.subplots()
ax.set_xlim(-1, 1)
ax.set_ylim(-1, 1)
ax.set_aspect("equal")

ax.plot([0], [0], "o", ms=30, c="gold")

# initial coords and vels
N_planets = 10
planet_coord = np.asarray([0.7, 0])
planet_vel = np.asarray([0, 0.01])
#planet_coord = np.random.uniform(low=-1, high=1, size=(N_planets, 2))
#planet_vel = np.random.uniform(low=0, high=1, size=(N_planets, 2))

sun_mass = 0.0002


line1, = ax.plot([],[], color = random_color(), marker = '.',ms=20)
ani = animation.FuncAnimation(fig, animate,
                              fargs=(line1, planet_coord,planet_vel,sun_mass),
                              interval=2,
                              blit=True)
plt.show()